// Теоретичні питання
// 1.
// 2.
// 3.
// 
// 
//Практичне завдання 
function createNewUser() {
  let firstName = prompt("Write your firstname");

  let lastName = prompt("Write your lastname");

  let birthdayDate = prompt("What is your birthday date?");

const birthdayParts = birthdayDate.split(".");
const birthday = new Date (
    +(birthdayParts[2]),
    +(birthdayParts[1]) - 1,
    +(birthdayParts[0]),
);

//   let now = new Date();

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,

    getAge: function () {
       const now = new Date();
       let age = now.getFullYear() - this.birthday.getFullYear();
       const monthDiff = now.getMonth() - this.birthday.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && now.getDate() < this.birthday.getDate())) {
        age--;
    }
       return age;

    },

    getLogin: function () {
      const firstLetter = this.firstName[0].toLowerCase();
      return firstLetter + this.lastName.toLowerCase();
    },

    getPassword: function () {
        const passwordFirstLetter = this.firstName[0].toUpperCase();
        const passwordLastName = this.lastName.toLowerCase();
        const passwordYear = this.birthday.getFullYear();
        return passwordFirstLetter + passwordLastName + passwordYear;
    }
  };
  return newUser;
}
const user = createNewUser();
const login = user.getLogin();
const password = user.getPassword();
const age = user.getAge();
console.log(login);
console.log(password);
console.log(age);


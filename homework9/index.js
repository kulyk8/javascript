function createList(arr, parent = document.body) {
  const ul = document.createElement("ul");
  arr.forEach((item) => {
    const li = document.createElement("li");
    li.textContent = item;
    ul.appendChild(li);
  });
  parent.appendChild(ul);
}
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
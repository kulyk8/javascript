document.addEventListener("DOMContentLoaded", function () {
  const buttons = document.querySelectorAll(".btn");

  
  let lastHighlightedButton = null;

  
  buttons.forEach((button) => {
    button.addEventListener("click", function () {
      
      if (lastHighlightedButton && lastHighlightedButton !== button) {
        lastHighlightedButton.style.backgroundColor = "#000000"; 
      }

     
      button.style.backgroundColor = "#0074D9";

     
      lastHighlightedButton = button;
    });
  });

  
  document.addEventListener("keydown", function (event) {
    const key = event.key.toUpperCase();

    const buttonWithKey = [...buttons].find(
      (button) => button.textContent === key
    );
    if (buttonWithKey) {
      buttonWithKey.click();
    }
  });
});

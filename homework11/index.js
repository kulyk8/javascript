
  document.addEventListener("DOMContentLoaded", function () {
    const passwordInputs = document.querySelectorAll('input[type="password"]');
    const passwordIcons = document.querySelectorAll(".icon-password");

    passwordIcons.forEach((icon, index) => {
      icon.addEventListener("click", function () {
        if (passwordInputs[index].type === "password") {
          passwordInputs[index].type = "text";
          icon.classList.remove("fa-eye");
          icon.classList.add("fa-eye-slash");
        } else {
          passwordInputs[index].type = "password";
          icon.classList.remove("fa-eye-slash");
          icon.classList.add("fa-eye");
        }
      });
    });

    const btnSubmit = document.querySelector(".btn");
    btnSubmit.addEventListener("click", function (event) {
      event.preventDefault();

      const firstPassword = passwordInputs[0].value;
      const secondPassword = passwordInputs[1].value;

      if (firstPassword === secondPassword) {
        // Паролі співпадають
        alert("You are welcome");
      } else {
        // Паролі не співпадають
        const errorMessage = document.createElement("span");
        errorMessage.textContent = "Потрібно ввести однакові значення";
        errorMessage.style.color = "red";
        const secondInputWrapper = document.querySelectorAll(".input-wrapper")[1];
        secondInputWrapper.appendChild(errorMessage);
      }
    });
  });


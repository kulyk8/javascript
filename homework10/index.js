document.addEventListener("DOMContentLoaded", function () {
  const tabTitles = document.querySelectorAll(".tabs-title");
  const tabContents = document.querySelectorAll(".tabs-content li");

  tabTitles.forEach((title, index) => {
    title.addEventListener("click", function () {
      // Видаляємо клас 'active' у всіх вкладок
      tabTitles.forEach((t) => t.classList.remove("active"));
      // Приховуємо весь контент
      tabContents.forEach((content) => (content.style.display = "none"));

      // Додаємо клас 'active' для вибраної вкладки
      title.classList.add("active");
      // Відображаємо відповідний контент
      tabContents[index].style.display = "block";
    });
  });
});

// Теоретичні питання
// 1.  Це функція що належить об'єкту.
// 2. string, number, boolean, array, object, null, function.
// 3. Це означає що він є контейнером, який містить дані та методи. він використовується з метою передачі інформації.
// 
// Практичне завдання.

function createNewUser(){
    let firstName = prompt("Write your firstname");
    
    let lastName = prompt("Write your lastname");
    
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin : function(){
            const firstLetter = this.firstName[0].toLowerCase();
            return firstLetter + this.lastName.toLowerCase();
        }
    }
    return newUser;
}
const user = createNewUser();
const login = user.getLogin();
console.log(login);
